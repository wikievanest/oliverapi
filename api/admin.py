from django.contrib import admin
from api.models import ApiSetting

# Register your models here.
admin.site.register(ApiSetting)