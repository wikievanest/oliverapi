from django.urls import path, include
from api.views import PatientList, PatientDetail
from api.views import InsuranceList, InsuranceDetail
from api.views import InsuranceCompanyList, InsuranceCompanyDetail, InsuranceAddressList, InsuranceAddressDetail
from api.views import EncounterList
from api.views import ProviderList, ProviderDetail
from rest_framework.authtoken import views

urlpatterns = [
    path("login/", views.obtain_auth_token, name="login"),
    path('patients/', PatientList.as_view(), name=PatientList.name),
    path('patients/<int:pk>/', PatientDetail.as_view(), name=PatientDetail.name),
    path('patients/<int:pid>/insurances/', InsuranceList.as_view(), name=InsuranceList.name),
    path('patients/<int:pid>/encounters/', EncounterList.as_view(), name=EncounterList.name),
    path('providers/', ProviderList.as_view(), name=ProviderList.name),
    path('providers/<int:pk>/', ProviderDetail.as_view(), name=ProviderDetail.name),
    path('patients/<int:pid>/insurances/<int:pk>/', InsuranceDetail.as_view(), name=InsuranceDetail.name),
    path('insurance-companies/', InsuranceCompanyList.as_view(), name=InsuranceCompanyList.name),
    path('insurance-companies/<int:pk>', InsuranceCompanyDetail.as_view(), name=InsuranceCompanyDetail.name),
    path('insurance-companies/<int:foreign_id>/address/', InsuranceAddressList.as_view(), name=InsuranceAddressList.name),
    path('insurance-companies/<int:foreign_id>/address/<int:pk>', InsuranceAddressDetail.as_view(), name=InsuranceAddressDetail.name),
]