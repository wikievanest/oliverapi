from rest_framework import serializers
from api.models import InsuranceCompanies, InsuranceAddress

class InsuranceAddressSerializer(serializers.ModelSerializer):

    #foreign_id = serializers.SlugRelatedField(
     #   queryset=InsuranceCompanies.objects.all(),
     #   slug_field='name')
    class Meta:
        model = InsuranceAddress
        read_only_fields = (
            'plus_four','id',)
        fields = (
                'id',
                'foreign_id',
                'line1',
                'line2',
                'city',
                'state',
                'zip',
                )

class InsuranceCompanySerializer(serializers.ModelSerializer):

    address = InsuranceAddressSerializer(many=True, read_only=True)

    class Meta:
        model = InsuranceCompanies
        read_only_fields = (
            'x12_receiver_id',
            'x12_default_partner_id',
            'alt_cms_id',
            'inactive')
        fields = (
                'id',
                'name',
                'attn',
                'cms_id',
                'ins_type_code',
                'address',
                )
