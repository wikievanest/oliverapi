from rest_framework import serializers
from api.models import InsuranceData
from api.serializers.insurance_company import InsuranceCompanySerializer

class InsuranceSerializer(serializers.ModelSerializer):

    class Meta:
        model = InsuranceData
        fields = (
                # 'url',
                'id',
                'pid',
                'type',
                'provider',
                'plan_name',
                'policy_number',
                'group_number',
                )

class InsuranceDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = InsuranceData
        fields = (
                # 'url',
                'id',
                'pid',
                'type',
                'provider',
                'plan_name',
                'policy_number',
                'group_number',
                'subscriber_lname',
                'subscriber_mname',
                'subscriber_fname',
                'subscriber_relationship',
                'subscriber_ss',
                'subscriber_dob',
                'subscriber_street',
                'subscriber_postal_code',
                'subscriber_city',
                'subscriber_state',
                'subscriber_country',
                'subscriber_phone',
                'subscriber_employer',
                'subscriber_employer_street',
                'subscriber_employer_postal_code',
                'subscriber_employer_state',
                'subscriber_employer_country',
                'subscriber_employer_city',
                'date',
                'subscriber_sex',
                'accept_assignment',
                'policy_type',
                )