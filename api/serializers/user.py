from rest_framework import serializers
from api.models import UserData

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserData
        fields = (
		# 'url',
                'id',
                'fname',
                'mname',
                'lname',
                'npi',
                'street',
                'city',
                'state',
                'zip',
                'phone',
                'active',
                )
