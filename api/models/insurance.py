from django.db import models
from api.models.patient import PatientData
from api.models.insurance_company import InsuranceCompanies
from api.utilities import get_list, get_max_pid, get_ins_company_list, utils

class InsuranceData(models.Model):
    
    def get_insurance_company():
        return sorted([
            (p.id, p.name) for p in InsuranceCompanies.objects.all()
        ])
    
    id = models.BigAutoField(primary_key=True)
    type = models.CharField(
        max_length=9,
        choices = get_list('insurance_types'))
    #provider = models.IntegerField(
    #    choices = get_insurance_company()
    #)
    provider = models.ForeignKey(
         InsuranceCompanies,
         related_name="insurances_company",
         on_delete=models.CASCADE,
         db_column='provider',
    )
    plan_name = models.CharField(max_length=255)
    policy_number = models.CharField(max_length=255)
    group_number = models.CharField(max_length=255)
    subscriber_lname = models.CharField(max_length=255, blank=True)
    subscriber_mname = models.CharField(max_length=255, blank=True)
    subscriber_fname = models.CharField(max_length=255, blank=True)
    subscriber_relationship = models.CharField(max_length=255, blank=True)
    subscriber_ss = models.CharField(max_length=255, blank=True)
    subscriber_dob = models.DateField(db_column='subscriber_DOB', blank=True)
    subscriber_street = models.CharField(max_length=255, blank=True)
    subscriber_postal_code = models.CharField(max_length=255, blank=True)
    subscriber_city = models.CharField(max_length=255, blank=True)
    subscriber_state = models.CharField(
        max_length=255,
        blank=True,
        choices = get_list('state'))
    subscriber_country = models.CharField(
        max_length=255,
        blank=True,
        choices = get_list('country'))
    subscriber_phone = models.CharField(max_length=255, blank=True)
    subscriber_employer = models.CharField(max_length=255, blank=True)
    subscriber_employer_street = models.CharField(max_length=255, blank=True)
    subscriber_employer_postal_code = models.CharField(max_length=255, blank=True)
    subscriber_employer_state = models.CharField(
        max_length=255,
        blank=True,
        choices = get_list('state'))
    subscriber_employer_country = models.CharField(
        max_length=255,
        blank=True,
        choices = get_list('country'))
    subscriber_employer_city = models.CharField(max_length=255, blank=True)
    copay = models.CharField(max_length=255, blank=True)
    date = models.DateField(auto_now_add=True)
    pid = models.ForeignKey(
        PatientData,
        related_name="pinsurances",
        to_field='pid',
        db_column='pid',
        on_delete=models.CASCADE
    )
    subscriber_sex = models.CharField(
        max_length=10,
        choices = get_list('sex'))
    accept_assignment = models.CharField(max_length=5, default='TRUE')
    policy_type = models.CharField(max_length=25, blank=True)

    class Meta:
        managed = False
        db_table = 'insurance_data'
