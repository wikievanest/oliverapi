from django.db import models

USE_TZ = True

class ApiSetting(models.Model):

    api_name = models.CharField(max_length=100)
    api_client = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'settings'

    def __str__(self):
        return self.api_name