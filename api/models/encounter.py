from django.db import models
from api.models import PatientData, UserData

class Encounter(models.Model):
    id = models.BigAutoField(primary_key=True)
    date = models.DateTimeField(blank=True, null=True)
    reason = models.TextField(blank=True, null=True)
    facility = models.TextField(blank=True, null=True)
    facility_id = models.IntegerField()
    pid = models.ForeignKey(
        PatientData,
        on_delete=models.CASCADE,
        related_name="encounters",
        db_column='pid'
    )
    encounter = models.BigIntegerField(blank=True, null=True)
    onset_date = models.DateTimeField(blank=True, null=True)
    sensitivity = models.CharField(max_length=30, blank=True, null=True)
    billing_note = models.TextField(blank=True, null=True)
    pc_catid = models.IntegerField()
    last_level_billed = models.IntegerField(default=0)
    last_level_closed = models.IntegerField(default=0)
    last_stmt_date = models.DateField(blank=True, null=True)
    stmt_count = models.IntegerField(default=0)
    provider_id = models.ForeignKey(
        UserData,
        on_delete=models.CASCADE,
        related_name="providers",
        to_field='id',
        db_column='provider_id'
    )
    supervisor_id = models.IntegerField(blank=True, null=True, default=0)
    invoice_refno = models.CharField(max_length=31)
    referral_source = models.CharField(max_length=31)
    billing_facility = models.IntegerField()
    external_id = models.CharField(max_length=20, blank=True, null=True)
    pos_code = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        managed = False
        db_table = 'form_encounter'

class Soap(models.Model):
    id = models.BigAutoField(primary_key=True)
    date = models.DateTimeField(blank=True, null=True)
    pid = models.ForeignKey(
        PatientData,
        on_delete=models.CASCADE,
        related_name="SOAP",
        db_column='pid'
    )
    user = models.CharField(max_length=255, blank=True, null=True)
    groupname = models.CharField(max_length=255, blank=True, null=True)
    authorized = models.IntegerField(blank=True, null=True)
    activity = models.IntegerField(blank=True, null=True)
    subjective = models.TextField(blank=True, null=True)
    objective = models.TextField(blank=True, null=True)
    assessment = models.TextField(blank=True, null=True)
    plan = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'form_soap'