from django.db import models
from api.utilities import utils

class PatientData(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(
        blank=True,
        max_length=25,
        choices = utils.get_list('titles'))
    language = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('language'))
    financial = models.CharField(max_length=255, blank=True)
    fname = models.CharField(max_length=255, blank=True)
    lname = models.CharField(max_length=255, blank=True)
    mname = models.CharField(max_length=255, blank=True)
    dob = models.DateField(db_column='DOB', blank=True)  # Field name made lowercase.
    street = models.CharField(max_length=255, blank=True)
    postal_code = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(
        max_length=255,
        blank=True,
        choices = utils.get_list('state'))
    country_code = models.CharField(
        max_length=255,
        blank=True,
        choices = utils.get_list('country'))
    drivers_license = models.CharField(max_length=255, blank=True)
    ss = models.CharField(max_length=255, blank=True)
    occupation = models.CharField(
        max_length=150,
        blank=True,
        choices = utils.get_list('Occupation'))
    phone_home = models.CharField(max_length=255, blank=True)
    phone_biz = models.CharField(max_length=255, blank=True)
    phone_contact = models.CharField(max_length=255, blank=True)
    phone_cell = models.CharField(max_length=255, blank=True)
    pharmacy_id = models.IntegerField(default=0)
    contact_relationship = models.CharField(max_length=255, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        max_length=10,
        blank=True,
        choices = utils.get_list('marital'))
    sex = models.CharField(
        max_length=10,
        blank=True,
        choices = utils.get_list('sex'))
    referrer = models.CharField(max_length=255, blank=True)
    referrerid = models.CharField(db_column='referrerID', max_length=255, blank=True)  # Field name made lowercase.
    providerid = models.IntegerField(db_column='providerID', default=0)  # Field name made lowercase.
    ref_providerid = models.IntegerField(db_column='ref_providerID', blank=True, default=0)  # Field name made lowercase.
    email = models.CharField(max_length=255, blank=True)
    email_direct = models.CharField(max_length=255, blank=True)
    ethnoracial = models.CharField(max_length=255, blank=True)
    race = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('race'))
    ethnicity = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('ethnicity'))
    religion = models.CharField(max_length=40, blank=True)
    interpretter = models.CharField(max_length=255, blank=True)
    migrantseasonal = models.CharField(max_length=255, blank=True)
    family_size = models.CharField(max_length=255, blank=True)
    monthly_income = models.CharField(max_length=255, blank=True)
    billing_note = models.CharField(max_length=255, blank=True)
    homeless = models.CharField(max_length=255, blank=True,)
    # financial_review = models.DateTimeField(default='0000-00-00 00:00:00')
    financial_review = models.DateTimeField(blank=True)
    pubpid = models.CharField(max_length=255, default=utils.get_max_pid)
    pid = models.BigIntegerField(unique=True, default=utils.get_max_pid)
    genericname1 = models.CharField(max_length=255, blank=True)
    genericval1 = models.CharField(max_length=255, blank=True)
    genericname2 = models.CharField(max_length=255, blank=True)
    genericval2 = models.CharField(max_length=255, blank=True)
    hipaa_mail = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_voice = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_notice = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_message = models.CharField(max_length=20, blank=True)
    hipaa_allowsms = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_allowemail = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    squad = models.CharField(max_length=32, blank=True)
    fitness = models.IntegerField(default=0, blank=True)
    referral_source = models.CharField(max_length=30, blank=True)
    usertext1 = models.CharField(max_length=255, blank=True)
    usertext2 = models.CharField(max_length=255, blank=True)
    usertext3 = models.CharField(max_length=255, blank=True)
    usertext4 = models.CharField(max_length=255, blank=True)
    usertext5 = models.CharField(max_length=255, blank=True)
    usertext6 = models.CharField(max_length=255, blank=True)
    usertext7 = models.CharField(max_length=255, blank=True)
    usertext8 = models.CharField(max_length=255, blank=True)
    userlist1 = models.CharField(max_length=255, blank=True)
    userlist2 = models.CharField(max_length=255, blank=True)
    userlist3 = models.CharField(max_length=255, blank=True)
    userlist4 = models.CharField(max_length=255, blank=True)
    userlist5 = models.CharField(max_length=255, blank=True)
    userlist6 = models.CharField(max_length=255, blank=True)
    userlist7 = models.CharField(max_length=255, blank=True)
    pricelevel = models.CharField(
        blank=True,
        max_length=50,
        default='standard',
        choices = utils.get_list('pricelevel'))
    regdate = models.DateTimeField(auto_now_add=True)
    contrastart = models.DateField(blank=True)
    completed_ad = models.CharField(max_length=3, default='NO')
    ad_reviewed = models.DateField(blank=True)
    vfc = models.CharField(max_length=255, blank=True)
    mothersname = models.CharField(max_length=255, blank=True)
    guardiansname = models.CharField(max_length=255, blank=True)
    allow_imm_reg_use = models.CharField(
        max_length=255,
        blank=True,
        choices = utils.get_list('yesno'))
    allow_imm_info_share = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    allow_health_info_ex = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    allow_patient_portal = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    # deceased_date = models.DateTimeField(default='0000-00-00 00:00:00')
    deceased_date = models.DateTimeField(blank=True)
    deceased_reason = models.CharField(max_length=255, blank=True)
    soap_import_status = models.IntegerField(blank=True)
    cmsportal_login = models.CharField(max_length=60, blank=True)
    care_team = models.IntegerField(blank=True, default=0)
    county = models.CharField(max_length=40, blank=True)
    industry = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('Industry'))
    imm_reg_status = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('immunization_registry_status'))
    imm_reg_stat_effdate = models.CharField(max_length=255, blank=True)
    publicity_code = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('publicity_code'))
    publ_code_eff_date = models.CharField(max_length=255, blank=True)
    protect_indicator = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    prot_indi_effdate = models.CharField(max_length=255, blank=True)
    guardianrelationship = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('next_of_kin_relathionship'))
    
    guardiansex = models.CharField(
        max_length=10,
        blank=True,
        choices = utils.get_list('sex'))
    guardianaddress = models.CharField(max_length=255, blank=True)
    guardiancity = models.CharField(max_length=255, blank=True)
    guardianstate = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('state'))
    guardianpostalcode = models.CharField(max_length=255, blank=True)
    guardiancountry = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('country'))
    guardianphone = models.CharField(max_length=255, blank=True)
    guardianworkphone = models.CharField(max_length=255, blank=True)
    guardianemail = models.CharField(max_length=255, blank=True)
    
    script_status = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    ccr_status = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    script_date = models.DateField(blank=True)
    ccr_date = models.DateField(blank=True)
    doc_count = models.CharField(max_length=255, blank=True)

    # Information
    lead_id = models.CharField(max_length=255, blank=True, null=True)
    agent_name = models.CharField(max_length=255, blank=True, null=True)
    original_date = models.CharField(max_length=255, blank=True, null=True)
    date_revised = models.CharField(max_length=255, blank=True, null=True)
    referring_doctor = models.CharField(max_length=255, blank=True, null=True)
    physical_exam_date = models.CharField(max_length=255, blank=True, null=True)
    chilhood_illness = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        choices = utils.get_list('Childhood_illness'))
    immunization = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        choices = utils.get_list('Immunizations_and_dates'))
    immunization_date = models.CharField(max_length=255, blank=True, null=True)
    any_medical_problem = models.CharField(max_length=255, blank=True, null=True)
    surgeries_year1 = models.CharField(max_length=255, blank=True, null=True)
    surgeries_reason1 = models.CharField(max_length=255, blank=True, null=True)
    surgeries_hospital = models.CharField(max_length=255, blank=True, null=True)
    surgeries_year2 = models.CharField(max_length=255, blank=True, null=True)
    surgeries_reason2 = models.CharField(max_length=255, blank=True, null=True)
    surgeries_hospital2 = models.CharField(max_length=255, blank=True, null=True)
    surgeries_year3 = models.CharField(max_length=255, blank=True, null=True)
    surgeries_reason3 = models.CharField(max_length=255, blank=True, null=True)
    surgeries_hospital3 = models.CharField(max_length=255, blank=True, null=True)
    hospitalizations1 = models.CharField(max_length=255, blank=True, null=True)
    hospital_reason1 = models.CharField(max_length=255, blank=True, null=True)
    hospital_hospital1 = models.CharField(max_length=255, blank=True, null=True)
    ever_had_blood = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        choices = utils.get_list('yesno'))
    name_the_drug1 = models.CharField(max_length=255, blank=True, null=True)
    strength1 = models.CharField(max_length=255, blank=True, null=True)
    frequency_taken1 = models.CharField(max_length=255, blank=True, null=True)
    name_the_drug2 = models.CharField(max_length=255, blank=True, null=True)
    strength2 = models.CharField(max_length=255, blank=True, null=True)
    frequency_taken2 = models.CharField(max_length=255, blank=True, null=True)
    name_the_drug_allerg = models.CharField(max_length=255, blank=True, null=True)
    drug_reaction1 = models.CharField(max_length=255, blank=True, null=True)
    name_the_drug_aller2 = models.CharField(max_length=255, blank=True, null=True)
    drug_reaction2 = models.CharField(max_length=255, blank=True, null=True)

    # FAMILY HEALTH HISTORY
    father_age1 = models.CharField(max_length=255, blank=True, null=True)
    father_problem = models.CharField(max_length=255, blank=True, null=True)
    mother_age = models.CharField(max_length=255, blank=True, null=True)
    mother_problem = models.CharField(max_length=255, blank=True, null=True)
    sibling_sex = models.CharField(
        blank=True,
        null=True,
        max_length=10,
        choices = utils.get_list('sex'))
    sibling_problem = models.CharField(max_length=255, blank=True, null=True)
    children_sex = models.CharField(
        blank=True,
        null=True,
        max_length=10,
        choices = utils.get_list('sex'))
    children_problem = models.CharField(max_length=255, blank=True, null=True)
    grandmother_age = models.CharField(max_length=255, blank=True, null=True)
    grandmother_problem = models.CharField(max_length=255, blank=True, null=True)
    grandfather_age = models.CharField(max_length=255, blank=True, null=True)
    grandfather_problem = models.CharField(max_length=255, blank=True, null=True)
    grandmother_pat_age = models.CharField(max_length=255, blank=True, null=True)
    grandmother_pat_prob = models.CharField(max_length=255, blank=True, null=True)
    grandfather_pat_age = models.CharField(max_length=255, blank=True, null=True)
    grandfather_pat_prob = models.CharField(max_length=255, blank=True, null=True)


    #BHealth Habits and personal saf
    Exercise = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        choices = utils.get_list('Exercise'))
    diet = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices=utils.get_list('yesno'))
    physician_medical = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    rank_salt_intake = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('Rank_Intake'))
    rank_fat_intake = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('Rank_Intake'))
    caffeine = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('Caffein'))
    cup_perday = models.CharField(max_length=255, blank=True, null=True)
    drink_alcohol = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    what_kind = models.CharField(max_length=255, blank=True, null=True)
    drink_per_week = models.CharField(max_length=255, blank=True, null=True)
    drink_amount = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    consider_stopping = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    experience_blackout = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    binge_drinking = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    drive_after_drink = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    use_tobacco = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    which_tobacco = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('Tobacco_List'))
    street_drugs = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    given_street_drugs = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    sexually_active = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    try_pregnancy = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    contraceptive_method = models.CharField(max_length=255, blank=True, null=True)
    discomfort_intercour = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    illness_related = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    live_alone = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    frequent_falls = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    vision_hear_loss = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    advance_directive = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    preparation_info = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    mental_abuse = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))

    # Mental Health

    stress_problem = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    feel_depressed = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    panic_stress = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    eat_problem = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    cry_frequently = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    attempted_suicide = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    seriuosly_thought = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    trouble_sleeping = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    to_conselor = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))

    # Women Only
    age_onset_mens = models.CharField(max_length=255, blank=True, null=True)
    date_last_mens = models.CharField(max_length=255, blank=True, null=True)
    how_many_day = models.CharField(max_length=255, blank=True, null=True)
    heavy_periods = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    number_of_pregnancy = models.CharField(max_length=255, blank=True, null=True)
    number_of_live = models.CharField(max_length=255, blank=True, null=True)
    is_pregnant = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    had_dc_hysterectomy = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    any_urinary_tract = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    any_blood = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    any_problems = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    any_hot_flash = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    have_mens_tension = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    recent_breast = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    date_last_pap = models.CharField(max_length=255, blank=True, null=True)

    # Men Only 
    urinate_the_night = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    how_many_urinate = models.CharField(max_length=255, blank=True, null=True)
    feel_pain_urination = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    blood_in_urine = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    feel_burn_penis = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    has_force_urination = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    had_any_kidney = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    any_problem_bladder = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    difficulty_erection = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    testicle_pain = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))
    last_prostate = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('yesno'))

    # Other Problem 
    any_symptoms = models.CharField(
        blank=True,
        null=True,
        max_length=255,
        choices = utils.get_list('Other_Problems'))


    def __str__(self):
        return str(self.pid) + ' - ' + self.fname + ' ' + self.lname

    class Meta:
        managed = False
        db_table = 'patient_data'
        ordering = ('-regdate', )
