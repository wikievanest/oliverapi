from django.db import models
from api.utilities import utils

class PatientData(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(
        blank=True,
        max_length=25,
        choices = utils.get_list('titles'))
    language = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('language'))
    financial = models.CharField(max_length=255, blank=True)
    fname = models.CharField(max_length=255, blank=True)
    lname = models.CharField(max_length=255, blank=True)
    mname = models.CharField(max_length=255, blank=True)
    dob = models.DateField(db_column='DOB', blank=True)  # Field name made lowercase.
    street = models.CharField(max_length=255, blank=True)
    postal_code = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(
        max_length=255,
        blank=True,
        choices = utils.get_list('state'))
    country_code = models.CharField(
        max_length=255,
        blank=True,
        choices = utils.get_list('country'))
    drivers_license = models.CharField(max_length=255, blank=True)
    ss = models.CharField(max_length=255, blank=True)
    occupation = models.CharField(
        max_length=150,
        blank=True,
        choices = utils.get_list('Occupation'))
    phone_home = models.CharField(max_length=255, blank=True)
    phone_biz = models.CharField(max_length=255, blank=True)
    phone_contact = models.CharField(max_length=255, blank=True)
    phone_cell = models.CharField(max_length=255, blank=True)
    pharmacy_id = models.IntegerField(default=0)
    contact_relationship = models.CharField(max_length=255, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        max_length=10,
        blank=True,
        choices = utils.get_list('marital'))
    sex = models.CharField(
        max_length=10,
        blank=True,
        choices = utils.get_list('sex'))
    referrer = models.CharField(max_length=255, blank=True)
    referrerid = models.CharField(db_column='referrerID', max_length=255, blank=True)  # Field name made lowercase.
    providerid = models.IntegerField(db_column='providerID', default=0)  # Field name made lowercase.
    ref_providerid = models.IntegerField(db_column='ref_providerID', blank=True, default=0)  # Field name made lowercase.
    email = models.CharField(max_length=255, blank=True)
    email_direct = models.CharField(max_length=255, blank=True)
    ethnoracial = models.CharField(max_length=255, blank=True)
    race = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('race'))
    ethnicity = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('ethnicity'))
    religion = models.CharField(max_length=40, blank=True)
    interpretter = models.CharField(max_length=255, blank=True)
    migrantseasonal = models.CharField(max_length=255, blank=True)
    family_size = models.CharField(max_length=255, blank=True)
    monthly_income = models.CharField(max_length=255, blank=True)
    billing_note = models.CharField(max_length=255, blank=True)
    homeless = models.CharField(max_length=255, blank=True,)
    # financial_review = models.DateTimeField(default='0000-00-00 00:00:00')
    financial_review = models.DateTimeField(blank=True)
    pubpid = models.CharField(max_length=255, default=utils.get_max_pid)
    pid = models.BigIntegerField(unique=True, default=utils.get_max_pid)
    genericname1 = models.CharField(max_length=255, blank=True)
    genericval1 = models.CharField(max_length=255, blank=True)
    genericname2 = models.CharField(max_length=255, blank=True)
    genericval2 = models.CharField(max_length=255, blank=True)
    hipaa_mail = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_voice = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_notice = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_message = models.CharField(max_length=20, blank=True)
    hipaa_allowsms = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    hipaa_allowemail = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    squad = models.CharField(max_length=32, blank=True)
    fitness = models.IntegerField(default=0, blank=True)
    referral_source = models.CharField(max_length=30, blank=True)
    usertext1 = models.CharField(max_length=255, blank=True)
    usertext2 = models.CharField(max_length=255, blank=True)
    usertext3 = models.CharField(max_length=255, blank=True)
    usertext4 = models.CharField(max_length=255, blank=True)
    usertext5 = models.CharField(max_length=255, blank=True)
    usertext6 = models.CharField(max_length=255, blank=True)
    usertext7 = models.CharField(max_length=255, blank=True)
    usertext8 = models.CharField(max_length=255, blank=True)
    userlist1 = models.CharField(max_length=255, blank=True)
    userlist2 = models.CharField(max_length=255, blank=True)
    userlist3 = models.CharField(max_length=255, blank=True)
    userlist4 = models.CharField(max_length=255, blank=True)
    userlist5 = models.CharField(max_length=255, blank=True)
    userlist6 = models.CharField(max_length=255, blank=True)
    userlist7 = models.CharField(max_length=255, blank=True)
    pricelevel = models.CharField(
        blank=True,
        max_length=50,
        default='standard',
        choices = utils.get_list('pricelevel'))
    regdate = models.DateTimeField(auto_now_add=True)
    contrastart = models.DateField(blank=True)
    completed_ad = models.CharField(max_length=3, default='NO')
    ad_reviewed = models.DateField(blank=True)
    vfc = models.CharField(max_length=255, blank=True)
    mothersname = models.CharField(max_length=255, blank=True)
    guardiansname = models.CharField(max_length=255, blank=True)
    allow_imm_reg_use = models.CharField(
        max_length=255,
        blank=True,
        choices = utils.get_list('yesno'))
    allow_imm_info_share = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    allow_health_info_ex = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    allow_patient_portal = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    # deceased_date = models.DateTimeField(default='0000-00-00 00:00:00')
    deceased_date = models.DateTimeField(blank=True)
    deceased_reason = models.CharField(max_length=255, blank=True)
    soap_import_status = models.IntegerField(blank=True)
    cmsportal_login = models.CharField(max_length=60, blank=True)
    care_team = models.IntegerField(blank=True, default=0)
    county = models.CharField(max_length=40, blank=True)
    industry = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('Industry'))
    imm_reg_status = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('immunization_registry_status'))
    imm_reg_stat_effdate = models.CharField(max_length=255, blank=True)
    publicity_code = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('publicity_code'))
    publ_code_eff_date = models.CharField(max_length=255, blank=True)
    protect_indicator = models.CharField(
        max_length=3,
        blank=True,
        choices = utils.get_list('yesno'))
    prot_indi_effdate = models.CharField(max_length=255, blank=True)
    guardianrelationship = models.CharField(
        max_length=100,
        blank=True,
        choices = utils.get_list('next_of_kin_relathionship'))
    
    guardiansex = models.CharField(
        max_length=10,
        blank=True,
        choices = utils.get_list('sex'))
    guardianaddress = models.CharField(max_length=255, blank=True)
    guardiancity = models.CharField(max_length=255, blank=True)
    guardianstate = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('state'))
    guardianpostalcode = models.CharField(max_length=255, blank=True)
    guardiancountry = models.CharField(
        max_length=50,
        blank=True,
        choices = utils.get_list('country'))
    guardianphone = models.CharField(max_length=255, blank=True)
    guardianworkphone = models.CharField(max_length=255, blank=True)
    guardianemail = models.CharField(max_length=255, blank=True)
    
    script_status = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    ccr_status = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    script_date = models.DateField(blank=True)
    ccr_date = models.DateField(blank=True)
    doc_count = models.CharField(max_length=255, blank=True)

    # Information
    #CGX
    genetic_test_cancer = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cancer_history = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))

    cancer_type_1 = models.CharField(max_length=255, blank=True)
    age_1 = models.CharField(max_length=255, blank=True)
    other_1 = models.CharField(max_length=255, blank=True)
    cancer_type_2 = models.CharField(max_length=255, blank=True)
    age_2 = models.CharField(max_length=255, blank=True)
    other_2 = models.CharField(max_length=255, blank=True)
    cancer_type_3 = models.CharField(max_length=255, blank=True)
    age_3 = models.CharField(max_length=255, blank=True)

    cgx_family_history = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))

    cgx_fam_cancer_1 = models.CharField(max_length=255, blank=True)
    cgx_fam_age_1 = models.CharField(max_length=255, blank=True)

    cgx_fam_gender_1 = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('sex'))
        
    cgx_fam_relathion_1 = models.CharField(max_length=255, blank=True)
    cgx_fam_side_1 = models.CharField(max_length=255, blank=True)
    cgx_fam_cancer_2 = models.CharField(max_length=255, blank=True)
    cgx_fam_age_2 = models.CharField(max_length=255, blank=True)

    cgx_fam_gender_2 = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('sex'))

    cgx_fam_relation_2 = models.CharField(max_length=255, blank=True)
    cgx_fam_side_2 = models.CharField(max_length=255, blank=True)
    cgx_fam_cancer_3 = models.CharField(max_length=255, blank=True)
    cgx_fam_age_3 = models.CharField(max_length=255, blank=True)

    cgx_fam_gender_3 = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('sex'))

    cgx_fam_relation_3 = models.CharField(max_length=255, blank=True)
    cgx_fam_side_3 = models.CharField(max_length=255, blank=True)

    # PGX

    pgx_blood_pressure = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    pgx_anxiety = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    pgx_prescription = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    pgx_two_medication = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    pgx_name_prescriptio = models.CharField(max_length=255, blank=True)
    # CARDIO AND DIABETES
    height = models.CharField(max_length=255, blank=True)
    weight = models.CharField(max_length=255, blank=True)
    bmi = models.CharField(max_length=255, blank=True)

    cd_diabetes = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))

    cd_type_diabetes = models.CharField(max_length=255, blank=True)
    cd_age_of_onset_1 = models.CharField(max_length=255, blank=True)

    cd_sleep_apnea = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))

    cd_age_of_onset_2 = models.CharField(max_length=255, blank=True)

    cd_knee_or_hip = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_gerd = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_retinopathy = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_neuropathy = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_sedentary = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_high_sugar = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_high_blood = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_high_cholesterol = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))

    cd_age_of_onset_3 = models.CharField(max_length=255, blank=True)

    cd_clogged_arteries = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_age_of_onset_4 = models.CharField(max_length=255, blank=True)

    cd_heart_disease = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_age_of_onset_5 = models.CharField(max_length=255, blank=True)

    cd_heart_attack = models.CharField(
        max_length=12,
        blank=True,
        choices = utils.get_list('yesno'))
    cd_age_of_onset_6 = models.CharField(max_length=255, blank=True)

    cd_heart_surgery = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_age_of_onset_7 = models.CharField(max_length=255, blank=True)

    cd_chest_pain = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_age_of_onset_8 = models.CharField(max_length=255, blank=True)

    cd_shortness_breath = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_age_of_onset_9 = models.CharField(max_length=255, blank=True)

    cd_other_heart = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_which_other = models.CharField(max_length=255, blank=True)
    cd_age_of_onset_10 = models.CharField(max_length=255, blank=True)

    cd_ecg = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_heart_beat = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_cardiomyopathy = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_anti_thrombotic = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_angina = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))
    cd_stroke = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('yesno'))

    cd_fam_relation_1 = models.CharField(max_length=255, blank=True)
    cd_fam_maternal_1 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('Maternal_Paternal'))
    cd_fam_overweight_1 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('Overweight'))
    cd_fam_gender_1 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('sex'))
    cd_fam_dx_1 = models.CharField(max_length=255, blank=True)
    cd_fam_age_1 = models.CharField(max_length=255, blank=True)

    cd_fam_relation_2 = models.CharField(max_length=255, blank=True)
    cd_fam_maternal_2 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('Maternal_Paternal'))
    cd_fam_overweight_2 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('Overweight'))
    cd_fam_gender_2 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('sex'))
    cd_fam_dx_2 = models.CharField(max_length=255, blank=True)
    cd_fam_age_2 = models.CharField(max_length=255, blank=True)

    cd_fam_relation_3 = models.CharField(max_length=255, blank=True)
    cd_fam_maternal_3 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('Maternal_Paternal'))
    cd_fam_overweight_3 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('Overweight'))
    cd_fam_gender_3 = models.CharField(
            max_length=12,
            blank=True,
            choices = utils.get_list('sex'))
    cd_fam_dx_3 = models.CharField(max_length=255, blank=True)
    cd_fam_age_3 = models.CharField(max_length=255, blank=True)
    best_time_to_call = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return str(self.pid) + ' - ' + self.fname + ' ' + self.lname

    class Meta:
        managed = False
        db_table = 'patient_data'
        ordering = ('-regdate', )
