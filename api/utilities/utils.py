from django.db import connection
from django.db import models

def get_list(param):
    """
        Get lists from list_options
    """
    c = connection.cursor()
    try:
        c.execute("SELECT option_id, title from list_options where list_id=%s", [param])
        results = c.fetchall()
        return results
    finally:
        c.close()
        
def get_max_pid():
    """
        Get max patient id
    """
    c = connection.cursor()
    try:
        c.execute("SELECT IFNULL(MAX(pid), 0)+1 from patient_data")
        results = c.fetchone()
        return results[0]
    finally:
        c.close()

def get_ins_company_list():
    """
        Get Insurance Companies lists
    """
    c = connection.cursor()
    try:
        c.execute("SELECT id, name from insurance_companies")
        results = c.fetchall()
        return results
    finally:
        c.close()

def get_max_ins_id():
    c = connection.cursor()
    try:
        c.execute("SELECT IFNULL(MAX(id), 0)+1 from insurance_companies")
        results = c.fetchone()
        return results[0]
    finally:
        c.close()

def get_max_ins_addr_id():
    c = connection.cursor()
    try:
        c.execute("SELECT IFNULL(MAX(id), 0)+1 from addresses")
        results = c.fetchone()
        return results[0]
    finally:
        c.close()

PAYER_TYPES = [
    (0, ''),
    (1, 'Other HCFA'),
    (2, 'Medicare Part B'),
    (3, 'Medicaid'),
    (4, 'ChampUSVA'),
    (5, 'ChampUS'),
    (6, 'Blue Cross Blue Shield'),
    (7, 'FECA'),
    (8, 'Self Pay'),
    (9, 'Central Certification'),
    (10, 'Other Non-Federal Programs'),
    (11, 'Preferred Provider Organization (PPO)'),
    (12, 'Point of Service (POS)'),
    (13, 'Exclusive Provider Organization (EPO)'),
    (14, 'Indemnity Insurance'),
    (15, 'Health Maintenance Organization (HMO) Medicare Risk'),
    (16, 'Automobile Medical'),
    (17, 'Commercial Insurance Co.'),
    (18, 'Disability'),
    (19, 'Health Maintenance Organization'),
    (20, 'Liability'),
    (21, 'Liability Medical'),
    (22, 'Other Federal Program'),
    (23, 'Title V'),
    (24, 'Veterans Administration Plan'),
    (25, 'Workers Compensation Health Plan'),
    (26, 'Mutually Defined'),
]

YESNO = [
    ('', ''),
    ('YES', 'YES'),
    ('NO', 'NO')
]