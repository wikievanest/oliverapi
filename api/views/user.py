from api.models import UserData
from api.serializers import UserSerializer
from rest_framework import generics
from rest_framework import permissions
from rest_framework import filters
from django_filters import NumberFilter, DateTimeFilter, AllValuesFilter

class EncounterList(generics.ListAPIView):

    def get_queryset(self):
        queryset = UserData.objects.all()
        return queryset

    serializer_class = UserSerializer
    name = 'user-list'

    permission_classes = (
        permissions.IsAuthenticated,
        )