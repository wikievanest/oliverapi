from api.models import PatientData
from api.serializers import PatientSerializer, PatientDetailSerializer
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from rest_framework import filters
from django_filters import NumberFilter, DateTimeFilter, AllValuesFilter
from rest_framework.response import Response

class PatientList(generics.ListCreateAPIView):
    
    queryset = PatientData.objects.all()
    serializer_class = PatientSerializer
    name = 'patient-list'

    permission_classes = (
        permissions.IsAuthenticated,
        )

    def create(self, request, *args, **kwargs):
        serializer = PatientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

class PatientDetail(generics.RetrieveUpdateAPIView):
    queryset = PatientData.objects.all()
    serializer_class = PatientDetailSerializer
    name = 'patient-detail'

    permission_classes = (
        permissions.IsAuthenticated,
        )
