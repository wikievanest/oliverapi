from api.models import InsuranceCompanies, InsuranceAddress
from api.serializers import InsuranceCompanySerializer, InsuranceAddressSerializer
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from rest_framework import filters
from django_filters import NumberFilter, DateTimeFilter, AllValuesFilter
from rest_framework.response import Response

class InsuranceCompanyList(generics.ListCreateAPIView):
    queryset = InsuranceCompanies.objects.all()
    serializer_class = InsuranceCompanySerializer
    name = 'insurance-company-list'

    permission_classes = (
        permissions.IsAuthenticated,
        )

    def create(self, request, *args, **kwargs):
        serializer = InsuranceCompanySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class InsuranceCompanyDetail(generics.RetrieveUpdateAPIView):
    queryset = InsuranceCompanies.objects.all()
    serializer_class = InsuranceCompanySerializer
    name = 'insurance-company-detail'

    permission_classes = (
        permissions.IsAuthenticated,
        )

class InsuranceAddressList(generics.ListCreateAPIView):

    def get_queryset(self):
        foreign_id = self.kwargs['foreign_id']
        queryset = InsuranceAddress.objects.filter(foreign_id=foreign_id)
        return queryset
    serializer_class = InsuranceAddressSerializer
    name = 'insurance-address-list'

    permission_classes = (
        permissions.IsAuthenticated,
        )

    def create(self, request, *args, **kwargs):
        serializer = InsuranceAddressSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class InsuranceAddressDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = InsuranceAddress.objects.all()
    serializer_class = InsuranceAddressSerializer
    name = 'insurance-address-detail'

    permission_classes = (
        permissions.IsAuthenticated,
        )