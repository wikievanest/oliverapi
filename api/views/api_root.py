from rest_framework import generics
from rest_framework.response import Response
from rest_framework.reverse import reverse

from api.models.patient import PatientData
from api.views import PatientList, PatientDetail
from api.views import InsuranceList, InsuranceDetail
from api.views import InsuranceCompanyList, InsuranceCompanyDetail, InsuranceAddressList, InsuranceAddressDetail

class ApiRoot(generics.GenericAPIView):

    """
    Returns a list of all **active** accounts in the system.

    For more details on how accounts are activated please [see here][ref].

    [ref]: http://example.com/activating-accounts
    """

    name = 'api-root'

    queryset = PatientData.objects.all()

    def get(self, request, *args, **kwargs):
        return Response({
            'patients': reverse(PatientList.name, request=request),
            'insurance-companies': reverse(InsuranceCompanyList.name, request=request),
            })
