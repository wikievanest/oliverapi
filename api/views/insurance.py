from api.models import InsuranceData
from api.serializers import InsuranceSerializer, InsuranceDetailSerializer
from rest_framework import generics
from rest_framework import permissions
from rest_framework import filters
from django_filters import NumberFilter, DateTimeFilter, AllValuesFilter

class InsuranceList(generics.ListCreateAPIView):

    def get_queryset(self):
        queryset = InsuranceData.objects.filter(pid=self.kwargs['pid'])
        return queryset

    serializer_class = InsuranceSerializer
    name = 'insurance-list'

    permission_classes = (
        permissions.IsAuthenticated,
        )

    

class InsuranceDetail(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        queryset = InsuranceData.objects.filter(id=self.kwargs['pk'], pid=self.kwargs['pid'])
        return queryset
    serializer_class = InsuranceDetailSerializer
    name = 'insurance-detail'

    permission_classes = (
        permissions.IsAuthenticated,
        )