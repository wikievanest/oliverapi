from api.models import Encounter
from api.serializers import EncounterSerializer
from rest_framework import generics
from rest_framework import permissions
from rest_framework import filters
from django_filters import NumberFilter, DateTimeFilter, AllValuesFilter

class EncounterList(generics.ListAPIView):

    def get_queryset(self):
        pid = self.kwargs['pid']
        queryset = Encounter.objects.filter(pid=pid)
        return queryset

    serializer_class = EncounterSerializer
    name = 'encounter-list'

    permission_classes = (
        permissions.IsAuthenticated,
        )