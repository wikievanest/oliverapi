from api.models import UserData
from api.serializers import UserSerializer
from rest_framework import generics
from rest_framework import permissions
from rest_framework import filters
from django_filters import NumberFilter, DateTimeFilter, AllValuesFilter

class ProviderList(generics.ListAPIView):

    def get_queryset(self):
        queryset = UserData.objects.filter(authorized=1)
        return queryset

    serializer_class = UserSerializer
    name = 'provider-list'

    permission_classes = (
        permissions.IsAuthenticated,
        )

class ProviderDetail(generics.RetrieveAPIView):

    def get_queryset(self):
        queryset = UserData.objects.all()
        return queryset

    serializer_class = UserSerializer
    name = 'provider-detail'

    permission_classes = (
        permissions.IsAuthenticated,
        )